FROM rasa/rasa:2.8.5-full
# this is run from the root folder of the repo
WORKDIR /app

USER 0
COPY requirements_actions.txt .
#RUN apt-get -y update
#RUN apt-get -y install git
RUN pip install -r requirements_actions.txt
COPY actions.py .
#RUN git clone https://github.com/BoseCorp/py-googletrans.git && cd py-googletrans && python setup.py install && cd ..

CMD [ "run", "actions", "--debug" ]
