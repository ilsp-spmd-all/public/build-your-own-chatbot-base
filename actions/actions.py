# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

import random
import requests

from typing import Any, Dict, List, Text

from googletrans import Translator
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher


class ActionInspirationalQuote(Action):
    def __init__(self, *args, **kwargs):
        super(ActionInspirationalQuote, self).__init__(*args, **kwargs)
        self.translator = Translator()

    def name(self) -> Text:
        return "action_inspireme"


    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:
        quote = requests.get("https://api.quotable.io/random").json()["content"]
        print(quote)
        #dispatcher.utter_message(text=quote)
        translated = self.translator.translate(quote, src="en", dest="el").text
        #dispatcher.utter_message(text="Άντε γεια")
        dispatcher.utter_message(text=translated)
        dispatcher.utter_message(text="Άντε γεια")

        return []

class ActionTranslate(Action):
    def __init__(self, *args, **kwargs):
        super(ActionTranslate, self).__init__(*args, **kwargs)
        self.translator = Translator()

    def name(self) -> Text:
        return "action_translate"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:
        # FIXME: Replace with COVID knowledge base response...
        bot_response = tracker.get_last_event_for("bot")["text"]
        translated = self.translator.translate([bot_response], src="en", dest="el")[0].text
        dispatcher.utter_message(text=translated)

        return []


class ActionCheerUp(Action):
    def name(self) -> Text:
        return "action_cheer_up"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(text="Here's smth to cheer you up")
        urls = ["http://google.com", "http://reddit.com", "http://facebook.com"]
        dispatcher.utter_message(text=random.choice(urls))

        return []

class ActionUtterGreetings(Action):
    ''' Use this to greet the user according to the time of day
    '''
    def name(self):
        return "action_utter_greetings"

    def run(self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # get hour of the day, rounded down (e.g. for 14.38 returnes 14)
        hour=datetime.utcnow().hour+2
        morning_hours=list(range(4,13))
        greeting="Γεια!"
        if hour in morning_hours:
            greeting="Καλημέρα!"
        else:
            greeting="Καλησπέρα!"
        dispatcher.utter_message(text=greeting)
        return []

class ActionUtterGoodbye(Action):
    ''' Use this to farewell the user according to the time of day
    '''
    def name(self):
        return "action_utter_goodbye"

    def run(self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # get hour of the day, rounded down (e.g. for 14.38 returnes 14)
        you_did_not_accept_terms="Οτιδήποτε άλλο εκτός από θετική απάντηση («Ναί») σημαίνει ότι δεν συναινείτε στους όρους χρήσης. Αν δεν συναινείτε στους όρους χρήσης δεν μπορείτε να χρησιμοποιήσετε το chatbot."
        hour=datetime.utcnow().hour + 2
        morning_hours=list(range(4,13))
        afternoon_hours=list(range(13,19))
        farewell="Γεια σας!"
        if hour in morning_hours:
            farewell="Καλή συνέχεια με την υπόλοιπη ημέρα σου!"
        elif hour in afternoon_hours:
            farewell="Καλό απόγευμα!"
        else: 
            farewell="Καληνύχτα!"
        dispatcher.utter_message(text=you_did_not_accept_terms)
        #dispatcher.utter_message(text=farewell)
        return []

